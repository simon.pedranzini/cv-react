import './Languages.scss'

const Languages = ({dataLanguages}) => {

    return (
        <>
            <div className="lg-general">
                <h1 className="line-title">Idiomas</h1>
                <div className="lg-container">
                    {dataLanguages.map((item) =>
                        <div className="lg-card" key={JSON.stringify(item.language)}>
                        <h5 className='lg-title'>{item.language}</h5>
                        <p>Nivel escrito: <span style={{fontWeight: 'bold'}}>{item.wrlevel}</span></p>
                        <p>Nivel hablado: <span style={{fontWeight: 'bold'}}>{item.splevel}</span></p>
                    </div>
                    )}
                </div>
            </div>
        </>
    )
}

export default Languages;