import './Education.scss'

const Education = ({ dataEducation }) => {
    // CUANDO LA PROP ES UN OBJETO, SE METE ENTRE LLAVES!

    return (
        <>
            <div className="e-container">
                <h1 className='line-title'>Educación</h1>
                {dataEducation.map((study) =>
                    <div className="e-card" key={JSON.stringify(study)}>
                        <p className='e-title'>{study.name}</p>
                        <p>{study.where} - {study.date}</p>
                    </div>
                )}
            </div>
        </>
    )
};

export default Education;