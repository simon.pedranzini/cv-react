import './AboutMe.scss'

const AboutMe = (props) => {

    const aboutMeArray = props.data.aboutMe;

    return (
        <>
            <div className="am-general">
            <h1 className='line-title'><span>Acerca de mi</span></h1>
            <div className="am-container">
                {aboutMeArray.map((hability) =>
                    <ul>
                        <li className="am-list" key={hability.id}>{hability.info}</li>
                    </ul>
                )}
            </div>
            </div>
        </>
    )
};

export default AboutMe;