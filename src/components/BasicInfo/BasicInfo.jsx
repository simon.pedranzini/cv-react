import './BasicInfo.scss'

const BasicInfo = (props) => {

    const data = props.data;
    // console.log(data)

    return (
        <>
        <div className="bi-general">
            <img className='bi-image' src={data.image} alt="Personal Image" />
                <h1 className='line-title'><span>Datos personales</span></h1>
            <div className="bi-container">
                <h2>{data.name} {data.lastName}</h2>
                <p>📍{data.city}</p>
                <p>📅{data.birthDate}</p>
                <p>
                    📩<a href={"mailto:" + data.email}>{data.email}</a>
                </p>
                <p>📱{data.phone}</p>
                <p>
                    🖥️<a target="_blank" href={data.gitLab}>GitLab</a>
                </p>
            </div>
        </div>
        </>
    )
};

export default BasicInfo;