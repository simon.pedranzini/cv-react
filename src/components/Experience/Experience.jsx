import './Experience.scss'

const Experience = ({dataExperience}) => {

    return (
        <>
        <div className="ex-container">
            <h1 className='line-title'><span>Experiencia</span></h1>
            {dataExperience.map((experience) =>
            <div className='ex-card' key={JSON.stringify(experience)}>
                <p className='e-title'>{experience.name}</p>
                <p>{experience.date}</p>
                <p>{experience.where}</p>
                <p>{experience.description}</p>
            </div>
            )}
        </div>
        </>
    )
};

export default Experience;