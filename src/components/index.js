import BasicInfo from "./BasicInfo/BasicInfo";
import AboutMe from "./AboutMe/AboutMe";
import Education from "./Education/Education";
import Experience from "./Experience/Experience";
import Languages from './Languages/Languages'

export { BasicInfo, AboutMe, Education, Experience, Languages }