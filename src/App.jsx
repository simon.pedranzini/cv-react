import './App.scss';
import { useState } from 'react';

import { BasicInfo, AboutMe, Education, Experience, Languages } from './components'
import { CV } from "./CV/CV";

const { basicInfo, education, experience, languages } = CV;

function App() {

  const [showEducation, setShowEducation] = useState(true);

  return (
    <div className="app">
      <h1 className='app-title'>Mi primer curriculum con REACT</h1>
      <BasicInfo data={basicInfo} />
      <AboutMe data={basicInfo} />
      <div className='btn-profesional'>
      <button className="btn-academy btn-info btn"
        onClick={() => setShowEducation(true)}>Educacion</button>
      <button className="btn-academy btn btn-info"
        onClick={() => setShowEducation(false)}>Experiencia</button>
        </div>
      <div>
        {showEducation ? (
        <Education dataEducation={education} />
        ) : (
        <Experience dataExperience={experience} />
        )}
      </div>
      <Languages dataLanguages={languages}/>
    </div>
  );
}

export default App;
