export const CV = {
    basicInfo: {
        name: "Simón",
        lastName: "Pedranzini González",
        adress: "Calle Pujades",
        city: "Barcelona",
        email: "hola@simonpedranzini.es",
        birthDate: "03/06/1995",
        phone: "(+34) 677305063",
        image: "https://clases-react.s3.eu-west-1.amazonaws.com/1624025332771.jpg",
        gitLab: "https://gitlab.com/simon.pedranzini",
        aboutMe: [{
                id: '1',
                info: "🤖 Techie desde la infancia.",
            },
            {
                id: '2',
                info: "🎮 Friki a tiempo parcial.",
            },
            {
                id: '3',
                info: "🏖️ Apasionado de la playa y el buen clima.",
            },
            {
                id: '4',
                info: "🚀 Con muchas ganas de despegar en el mundo del desarollo.",
            },
        ],
    },
    education: [{
            name: "📒 Sistemas Microinformaticos y Redes",
            date: "2015",
            where: "Blanes",
        },
        {
            name: "📓 Administración de Sistemas Informáticos en Red",
            date: "2018",
            where: "Barcelona",
        },
        {
            name: "🖥️ FullStack Developer",
            date: "2021",
            where: "Online",
        },
    ],
    experience: [{
            name: "⌨️ Consultant IT",
            date: "Octubre, 2018 – Actualidad",
            where: "Tecnologia Familiar - Barcelona",
            description: "Mantenimiento, Servicio y Asistencia de Sistemas y Redes informáticas",
        },
        {
            name: "📨 Customer Service - Agent",
            date: "Octubre, 2019 – Febrero 2020",
            where: "Digamelon - Barcelona",
            description: "Atención al cliente en plataforma Zendesk y gestión de eComerce como Shopify o PrestaShop.",
        },
        {
            name: "🍹 Camarero de Barra",
            date: "Junio, 2015 – Septiembre 2017",
            where: "Restaurante el Romaní - Lloret de MAr",
            description: "",
        }
    ],
    languages: [{
        language: "Español",
        wrlevel: "Nativo",
        splevel: "Nativo",
    }, {
        language: "Catalan",
        wrlevel: "Alto",
        splevel: "Alto",
    }, {
        language: "Ingles",
        wrlevel: "Basico",
        splevel: "Basico",
    },
],
    habilities: [
        "Programación",
        "Manejo de diferentes tecnologias",
        "Apasionado por la informática",
        "Majero de herramientas de ticketing",
    ],
    volunteer: [{
            name: "Voluntariado con gente mayor",
            where: "Cruz Roja",
            description: "Acompañamiento de personas mayores en riesgo de exclusión",
        },
        {
            name: "Limpieza marina",
            where: "Costa Brava",
            description: "Inmersión marina para la limpieza del fondo de diferentes playas de la Costa Brava.",
        },
    ],
};